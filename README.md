ESTE PLUGIN NÃO É MEU, APENAS CRIANDO REPOSITÓRIO PARA VERSIONAMENTO.

ENGLISH: THIS PLUGIN IS NOT MINE. JUST A REPO TO VERSIONATE.

ORIGINAL THREAD: https://forums.alliedmods.net/showthread.php?t=165895
Thanks to ["Kemsan"](https://github.com/Kemsan)


---

# Event Sounds

### Description:
Plugin add feature to push sound to client after run each game event. Works with all game, and all game events. Plugin are very very easy to conifgure. Auto precache, download and play random sound. It's easy, it's here! Build round sound - easly. Enter sounds, end round sounds, start round sounds, kill sounds - all in one, easly!

### Cvars:
`sm_event_sounds_version` - Version of plugin

`sm_event_sounds_play_type` Default "SOUND_PLAYTYPE" value for each event; 1 - use client based command ( play SOUND ), 2 - use game sound effect

`sm_event_sounds_notify_msg` Default "NOTIFY_MSG" value for each event; {SOUND_NAME} - actual played sound

`sm_event_sounds_notify_delay` Default "NOTIFY_MSG_DELAY" value for each event; Delay before show notify, 0.0 turn off notify show

`sm_event_sounds_sound_delay` Default "NOTIFY_SOUND_DELAY" value for each event; Delay before play sound; Minimum 0.1

`sm_event_sounds_color_notify` Use special coloring function for notify? 0 - disable, 1 - enable

`sm_event_sounds_clientonly`
Default "SOUND_CLIENTONLY" value for each event; 0 - push sound to client and all ( if need ), 1 - push always to client ( if exists )

### Chat commands:
`!stop`, `/stop` - stop actual playing sound
`!stopall`, `/stopall` - stop all sounds, now playing and others in the future

### Key values help:
Example event (player_spawn):
```KeyValue
"player_spawn"
    {
        //Conditions
        "conditions"
        {
            //First - event value name, sec (after : ) - value type (string,int,float,userid). Value - condition value ( userid  = 1 ); ex: weapon:string - scattergun - if weapon are scattergun, plugin push sound.
            "userid:userid"     "1"
        }
        //Sounds
        "sounds"
        {
            "Warioware Intro Sound"     "sound/MySoundFolder/sample.mp3" //First element - sound name, next element - sound path
            "Other sound"     "sound/MySoundFolder/OtherSound.mp3" //First element - sound name, next element - sound path
        }
        //Options
        "options"
        {
            "NOTIFY_MSG"    "{olive}Hello camrat! You hear {teamcolor}{SOUND_NAME}!" //Sound notify message
            "NOTIFY_MSG_DELAY"    "0.1" //Show notify message after this time
            "NOTIFY_SOUND_DELAY"    "0.1" //Play sound to client / all clients after this time
            "NOTIFY_PUSH_SOUND" "1" //Allow or disallow to push sound
            "SOUND_PLAYTYPE"    "1" //"Playtype" for actual event - 1 - use client commd (play), 2 - use sdk sound function (emtisound)
            "SOUND_CLIENTONLY" "1" //0 - play all clients ( if can ), 1 - play sound only for client ( if exists )
        }
    } 
```

### Event list:
- [Generic Source](http://wiki.alliedmods.net/Generic_Source_Events)
- [Counter-Strike: Source](http://wiki.alliedmods.net/Counter-Strike:_Source_Events)
- [Day of Defeat: Source](http://wiki.alliedmods.net/Day_of_Defeat:_Source_Events)
- [Half-Life 2: Deathmatch](http://wiki.alliedmods.net/Half-Life_2:_Deathmatch_Events)
- [Half-Life 2: Capture the Flag](http://wiki.alliedmods.net/Half-Life_2:_Capture_the_Flag_Events)
- [Dystopia](http://wiki.alliedmods.net/Dystopia_Events)
- [Pirates, Vikings, Knights II](http://wiki.alliedmods.net/Pirates,_Vikings,_Knights_II_Events)
- [SourceForts](http://wiki.alliedmods.net/SourceForts_Events)
- [Hidden: Source](http://wiki.alliedmods.net/Hidden:_Source_Events)
- [Perfect Dark: Source](http://wiki.alliedmods.net/Perfect_Dark:_Source_Events)
- [Iron Grip: Source](http://wiki.alliedmods.net/Iron_Grip:_Source_Events)
- [Insurgency: Source](http://wiki.alliedmods.net/Insurgency:_Source_Events)
- [Zombie Panic! Source](http://wiki.alliedmods.net/Zombie_Panic%21_Source_Events)
- [Garry's Mod](http://wiki.alliedmods.net/Garry%27s_Mod_Events)
- [Team Fortress 2](http://wiki.alliedmods.net/Team_Fortress_2_Events)
- [Left 4 Dead](http://wiki.alliedmods.net/Left_4_Dead_Events)
- [Left 4 Dead 2](http://wiki.alliedmods.net/Left_4_Dead_2_Events)
- [Neotokyo](http://wiki.alliedmods.net/Neotokyo_Events)
- [Alien Swarm](http://wiki.alliedmods.net/Alien_Swarm_Events)
- [Nuclear Dawn](http://wiki.alliedmods.net/Nuclear_Dawn_Events)

### Changelog:
- Version 1.0 (28.08.2011)
  - Initial/Public Release
- Version 1.1 (29.08.2011)
  - Added two client commands
  - !stop, /stop - stop actual playing sound
  - !stopall, /stopall - stop all sounds, now and in the future
- Version 1.2 (16.10.2011)
  - Changed hook event system - hook only used events
  - Added conditions
  - Added only show notify option

### Installation:
 - Put **event_sounds.smx** to _sourcemod/plugins_
 - Put **event_sounds.sp** to _sourcemod/scripting_
 - Put **event_sounds.keys.txt** to _sourcemod/configs_

In .zip you find sample of event_sounds.keys.txt
Wait for change map or write in rcon - sm plugins load event_sounds

### Plugin using:
- [exvel](http://forums.alliedmods.net/member.php?u=17252) [INC] Colors